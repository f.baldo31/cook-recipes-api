export enum Difficulty {
    EASY = 'Facile',
    MEDIUM = 'Moyen',
    HARD = 'Difficile'
}

export const UNITS = {
    'gramme': 'g',
    'kilo': 'kg',
    'millilitre': 'ml',
    'centilitre': 'cl',
    'décilitre': 'dl',
    'litre': 'L',
    'pincée': 'p',
    'cuillère à soupe': 'c a s',
    'cuillère à café': 'c a c',
    'tasse': 't',
    'pièce': '',
};
